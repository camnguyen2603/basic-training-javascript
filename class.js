class Animal{
    constructor(name,age,spec){
        this.name = name;
        this.age = age;
        this.spec = spec;
    }
    speak(){
        return "meow meow";
    }
    get getAge(){
        return this.age;
    }
    static sumCatAge(cat1 ,cat2){ 
        return cat1.age + cat2.age;
    }
}

let cat1 = new Animal("Tom" , 2 , "cat");
let cat2 = new Animal("paw" , 4 , "cat");

console.log(cat1);
//check cat 1 
cat1 instanceof Animal 
//call method speak()
console.log(cat1.speak());
//console.log(cat1.getAge);

console.log(Animal.sumCatAge(cat1,cat2));
Animal.prototype.color = "black";
console.log(cat1.hasOwnProperty("color"));


 
class Dog extends Animal{
  constructor(name, age ,gender, spec="dog"){
      super(name , age ,spec);
      this.gender = gender;
  }
}
let dog1  = new Dog("Pu" ,2 ,"male")
console.log(dog1);
console.log(dog1.hasOwnProperty("name"));

